/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;
        
/**
 *
 * @author informatics
 */
public class Lab2 {

    private static final char[][] Board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'x';
    private static int row, col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printBoard();
            printTurn();
            inputRowCol();
            if (isWin()) {
                if (!playAgain()) {
                    break;
                }
                resetBoard();
            }
            switchPlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row,col :");
            row = kb.nextInt();
            col = kb.nextInt();
            if (Board[row - 1][col - 1] == '-') {
                Board[row - 1][col - 1] = currentPlayer;
                return;
            }
            switchPlayer();
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'x') {
            currentPlayer = 'o';
        } else {
            currentPlayer = 'x';
        }
    }

    //CheckWin
    private static boolean isWin() {
        if (checkRow() || checkCol() || checkX()) {
            printBoard();
            System.out.println(currentPlayer + " Win !");
            return true;
        } else if (checkDraw()) {
            printBoard();
            System.out.println("Draw!!");
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (Board[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (Board[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if ((Board[0][0] == currentPlayer && Board[1][1] == currentPlayer && Board[2][2] == currentPlayer) || (Board[0][2] == currentPlayer && Board[1][1] == currentPlayer && Board[2][0] == currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean playAgain() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to play new game? (Y/N): ");
        String input = kb.nextLine();
        return input.equalsIgnoreCase("Y");
    }

    private static void resetBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Board[i][j] = '-';
            }
        }
    }
}
